package POST_METHOD;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

class rest_post_BDD {

	public static Logger log = LogManager.getLogger(rest_post_BDD.class.getName());

	@BeforeClass
	public void setBaseURI(){
		RestAssured.baseURI ="http://restapi.demoqa.com/customer";
	}
	
	@Test
	public void TC_1_RegistrationSuccessful()
	{		
		Response res=
		given()
		.param("FirstName", "Virender")
		.param("LastName", "Singh")
		.param("UserName", "sdimpleuser2dd2011")
		.param("Password", "password1")
		.param("Email",  "sample2ee26d9@gmail.com")
	 
		.when()
		.post("/register")
	 
		.then ()
		.assertThat().statusCode(200)
		//.assertThat().statusCode(400)
		.contentType(ContentType.JSON)
	    .extract().response();
	    log.info(res.asString());
	}	
}
