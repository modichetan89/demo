package GET_METHOD;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class GoogleMap_Get {
	
	//Logging implementation
	public static Logger log = LogManager.getLogger(GoogleMap_Get.class.getName());

	@BeforeClass
	public void setBaseURI(){
		RestAssured.baseURI = "https://maps.googleapis.com";
	}
	
	@Test(priority=2)
	public void TC_3_testStatusCodeRestAssured(){
		//Response res;
		given()
		.param("query","pune")
		.param("key","AIzaSyD5qbsbCi6gdJz69AOBGYlCi4vWuVvnfjY")
		
		.when()
		.get("/maps/api/place/textsearch/json")
		
		.then ()
		.assertThat().statusCode(200).body("results[0].name", is("Pune"));          				
	}
	
	@Test(priority=1)
	public void TC_2_testJSONExtractRestAssured(){
		Response res=
		given()
		.param("query","pune")
		.param("key","AIzaSyD5qbsbCi6gdJz69AOBGYlCi4vWuVvnfjY")
		
		.when()
		.get("/maps/api/place/textsearch/json")
		
		.then ()
		.contentType(ContentType.JSON)
	    .extract().response();
	    log.info(res.asString());
	    log.info(res.getHeaders());
	}
}


//https://maps.googleapis.com/maps/api/place/textsearch/json?query=gurgaon&key=AIzaSyD5qbsbCi6gdJz69AOBGYlCi4vWuVvnfjY